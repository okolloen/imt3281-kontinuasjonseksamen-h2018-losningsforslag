package application;

/**
 * Sample Skeleton for 'GameOfLife.fxml' Controller Class
 */

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class GameOfLife {
	private boolean running = false;
	private no.ntnu.imt3281.gameOfLife.GameOfLife game = null;
	private int nrows, ncols;
	private double tileWidth, tileHeight;
	private Rectangle[][] visibleRects;
	private Timer timer;

	@FXML // fx:id="gameOfLife"
    private AnchorPane gameOfLife; // Value injected by FXMLLoader
	
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;
    
    @FXML
    private Label generation;

    @FXML // fx:id="speed"
    private ComboBox<String> speed; // Value injected by FXMLLoader

    @FXML // fx:id="rows"
    private TextField rows; // Value injected by FXMLLoader

    @FXML // fx:id="cols"
    private TextField cols; // Value injected by FXMLLoader

    @FXML
    void pause(ActionEvent event) {
    		if (!running)	// Not running
    			return;
    		timer.cancel();
    		running = false;
    }

    @FXML
    void reset(ActionEvent event) {
    		if (running)
    			return;
    		gameOfLife.getChildren().clear();
    		nrows = Integer.parseInt(rows.getText());
        ncols = Integer.parseInt(cols.getText());
        game = new no.ntnu.imt3281.gameOfLife.GameOfLife(nrows, ncols);
        generation.setText("0");
        drawGrid();
    }

    @FXML
    void start(ActionEvent event) {
    		if (running)		// Already running
    			return;
    		running = true;
    		timer = new Timer();
    		TimerTask animation = new TimerTask() {
    			public void run() {
    				game.computeNextGeneration();
    	    			Platform.runLater(()-> {
					generation.setText(Integer.toString(Integer.parseInt(generation.getText())+1));
					updateGrid();
                });
			}
    		};
    		int selectedSpeed = speed.getSelectionModel().getSelectedIndex();
    		timer.schedule(animation, 0, selectedSpeed==0?2000:selectedSpeed==1?1000:500);
    }

    @FXML
    void step(ActionEvent event) {
    		if (running)
    			return;
    		game.computeNextGeneration();
    		generation.setText(Integer.toString(Integer.parseInt(generation.getText())+1));
    		updateGrid();
    }
    
    @FXML
    void changeCell(MouseEvent event) {
    		if (running) {	// Simulation is running, we can't change cells while simulation is running
    			return;
    		}
    		int x = (int) (event.getX()/tileWidth);
    		int y = (int) (event.getY()/tileHeight);
    		
    		if (game.isAlive(y, x)) {
    			game.setDeadCell(y, x);
    		} else {
    			game.setLivingCell(y, x);
    		}
    		updateGrid();
    }

    private void updateGrid() {
		for (int y=0; y<nrows; y++) {
			for (int x=0; x<ncols; x++) {
				if (game.isAlive(y, x)) {
					visibleRects[y][x].setFill(Color.BLACK);
				} else {
					visibleRects[y][x].setFill(Color.TRANSPARENT);
				}
			}
		}
	}

	@FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert speed != null : "fx:id=\"speed\" was not injected: check your FXML file 'GameOfLife.fxml'.";
        assert rows != null : "fx:id=\"rows\" was not injected: check your FXML file 'GameOfLife.fxml'.";
        assert cols != null : "fx:id=\"cols\" was not injected: check your FXML file 'GameOfLife.fxml'.";
        nrows = Integer.parseInt(rows.getText());
        ncols = Integer.parseInt(cols.getText());
        game = new no.ntnu.imt3281.gameOfLife.GameOfLife(nrows, ncols);
        generation.setText("0");
        speed.getItems().addAll(
                "0.5 generasjoner pr. sekund",
                "1 generasjon pr. sekund",
                "2 generasjoner pr. sekund"
            );
        speed.getSelectionModel().select(1);
    }

	public void drawGrid() {
        double width = gameOfLife.getWidth();
        double height = gameOfLife.getHeight();
        
        tileWidth = width/ncols;
        tileHeight = height/nrows;
        
        visibleRects = new Rectangle[nrows][ncols];
        
        for (int x=0; x<ncols; x++) {
        		for (int y=0; y<nrows; y++) {
        			Rectangle r = new Rectangle(tileWidth, tileHeight);
        			r.setFill(Color.TRANSPARENT);

        		    r.setStroke(Color.BLACK);
        		    gameOfLife.getChildren().add(r);
        		    r.setTranslateX(x*tileWidth);
        		    r.setTranslateY(y*tileHeight);
        		    visibleRects[y][x] = r;
        		}
        }
	}
}
