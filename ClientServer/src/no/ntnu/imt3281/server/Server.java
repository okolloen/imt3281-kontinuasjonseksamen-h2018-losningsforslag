package no.ntnu.imt3281.server;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Server {
	ArrayList<Connection> clients = new ArrayList<>();
	BlockingQueue<Message> messages = new ArrayBlockingQueue<>(50);
	
	boolean stopping = false;
	
	public Server() {
		try {
			ServerSocket server = new ServerSocket(4444);
			new Thread (()-> {	// Listen for connections in a separate thread
				while (!stopping) {
					Socket s;
					try {
						s = server.accept();
						synchronized (clients) {
							clients.add(new Connection(s));
						}
					} catch (IOException e) {	// Problems creating streams to/from the client
						e.printStackTrace();
					}
				}
				try {
					server.close();
				} catch (IOException e) {	// Unable to stop the server
					e.printStackTrace();
				}
			}).start();
			new Thread(()-> {	// Listen for incomming messages in a separate thread
				while (!stopping) {
					for (int i=0; i<clients.size(); i++) {
						if (clients.get(i)!=null) {
							try {
								Object o = clients.get(i).read();
								if (o instanceof String && o.toString().equals("SHUTDOWN")) {
									clients.get(i).close();
									clients.set(i, null);
								} else if (o!=null) {
									messages.add(new Message(i, o));
								}
							} catch (ClassNotFoundException e) {
								clients.set(i, null);
							} catch (IOException e) {
								clients.set(i, null);
							}
						}
					}
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}).start();
			new Thread(()->{		// Distribute messages to clients in a separate thread
				while (!stopping) {
					if (messages.size()>0) {
						Message m = null;
						try {
							m = messages.take();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						for (int i=0; i<clients.size(); i++) {
							if (i!=m.getClient()&&clients.get(i)!=null) {	// Don't send to self and disconnected clients
								try {
									clients.get(i).write(m.getMessage());
								} catch (IOException e) {
									System.out.println("client "+i+" gone, setting to null");
									clients.set(i, null);
								}
							}
						}
					}
				}
			}).start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	class Message {
		private int client;
		private Object msg;
		
		public Message (int client, Object msg) {
			this.client = client;
			this.msg = msg;
		}
		
		public int getClient() {
			return client;
		}
		
		public Object getMessage() {
			return msg;
		}
	}
	
	class Connection {
		private Socket s;
		private ObjectInputStream ois;
		private BufferedInputStream bis;
		private ObjectOutputStream oos;
		
		public Connection(Socket s) throws IOException {
			this.s = s;
			ois = new ObjectInputStream(s.getInputStream());
			bis = new BufferedInputStream(s.getInputStream());
			oos = new ObjectOutputStream(s.getOutputStream());
		}
		
		public Object read() throws ClassNotFoundException, IOException {
			if (bis.available()>0) {
				return ois.readObject();
			} else {
				return null;
			}
		}
		
		public void write(Object o) throws IOException {
			System.out.println("writing '"+o+"' to client "+s.toString());
			oos.writeObject(o);
			oos.flush();
		}
		
		public void close() {
			try {
				write("TERMINATE");
				ois.close();
				oos.close();
				s.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		new Server();
	}
}

